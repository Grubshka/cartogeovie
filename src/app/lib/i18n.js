import { addMessages, init } from "svelte-i18n"

import en from "../../locales/en.json"
import fr from "../../locales/fr.json"

import configEn from "../config/locales/en.json"
import configFr from "../config/locales/fr.json"

addMessages("en", en)
addMessages("fr", fr)

addMessages("en", configEn)
addMessages("fr", configFr)

export default init
