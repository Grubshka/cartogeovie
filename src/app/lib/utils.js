function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomFrenchCoords() {
  return {
    lat: getRandomInt(43000000, 50000000)/1000000,
    lng: -getRandomInt(2000000, 4000000)/1000000
  }
}

function formatAddress(address) {
  let str = ""
  if (address.road) str += address.road
  if (address.hamlet) str += (str ? " - " : "") + address.hamlet
  str += " " + address.postcode + " " + (address.village || address.town || address.municipality)
  return str
}

export { getRandomInt, getRandomFrenchCoords, formatAddress }