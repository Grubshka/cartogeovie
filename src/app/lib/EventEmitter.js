class EventEmitter {
  constructor() {
    this.events = {}
  }

  on(event, listener) {
    if (typeof this.events[event] !== 'object') {
      this.events[event] = []
    }

    this.events[event].push(listener)
  }

  removeListener(event, listener) {
    let idx

    if (typeof this.events[event] === 'object') {
      idx = indexOf(this.events[event], listener)

      if (idx > -1) {
        this.events[event].splice(idx, 1)
      }
    }
  }
  emit(event) {
    let i, listeners, length, args = [].slice.call(arguments, 1)

    if (typeof this.events[event] === 'object') {
      listeners = this.events[event].slice()
      length = listeners.length

      for (let i = 0; i < length; i++) {
        try {
          listeners[i].apply(this, args)
        }
        catch(e) {
          console.log(e)
        }
      }
    }
  }

  once(event, listener) {
    this.on(event, function g() {
      this.removeListener(event, g)
      listener.apply(this, arguments)
    })
  }
}

export default EventEmitter