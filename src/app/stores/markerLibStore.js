import { localStore } from './localStore.js'

const initialMarkerLib = [ ]

class MarkerLibStore {

  #store

  constructor(initialMarkers) {
    this.markerLib = []
    this.#store = localStore('ladapt-marker-library', initialMarkerLib)
    this.#store.subscribe(value => {
      this.markerLib = value || []
    })
  }

  subscribe() {
    return this.#store.subscribe.apply(this.#store, arguments)
  }

  update() {
    return this.#store.update.apply(this.#store, arguments)
  }

  set() {
    return this.#store.set.apply(this.#store, arguments)
  }

  clear() {
    return this.#store.set.apply(this.#store, initialMarkerLib)
  }

  all() {
    return this.markerLib || []
  }

  ids() {
    return this.markerLib.map((m) => m.id)
  }

  get(id) {
    return this.markerLib.find(m => m.id === id)
  }

  add(id, markers) {
    let mid = id || this._getNewId()
    let markerSet = { id: mid, markers: markers }
    this.markerLib = [...this.markerLib, markerSet] 
    this._updated()
    return mid
  }

  remove(id) {
    this.markerLib = this.markerLib.filter(m => m.id !== id)
    this._updated()
  }

  update(id, markers) {
    const i = this.markerLib.findIndex(m => m.id === id)
    this.markerLib[i] = { id: id, markers: markers }
    this._updated()
  }

  replace(id, markers) {
    const i = this.markerLib.findIndex(m => m.id === id)
    this.markerLib[i] = { id: id, markers: markers }
    this._updated()
  }

  export() {
    return JSON.stringify(this.markerLib)
  }

  import(markerLib) {
    this.markerLib = typeof(markerLib) == 'string' ? JSON.parse(markerLib) : markerLib
  }

  /**
   * 
   * @param {*} markerSet 
   * @returns true if equals 
   */
  isModified(id, markers) {
    let markerSet = this.get(id)
    return !(markerSet && (JSON.stringify(markerSet.markers) == JSON.stringify(markers)))
  }

  _getNewId() {
    return "Market set " + (this.markerLib.length > 0 ? Math.max(...this.markerLib.map(m => m.id)) + 1 : 1)
  }

  _updated() {
    this.#store.set(this.markerLib)
  }
}

const markerLibStore = new MarkerLibStore()

export default markerLibStore