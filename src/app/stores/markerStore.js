import { localStore } from './localStore.js'
import EventEmitter from '../lib/EventEmitter';

const initialMarkers = [ ]

class MarkerStore extends EventEmitter {

  #store

  constructor(initialMarkers) {
    super()
    this.markers = []
    this.#store = localStore('ladapt-current-markers', initialMarkers)
    this.#store.subscribe(value => {
      this.markers = value || []
    })
  }

  subscribe() {
    return this.#store.subscribe.apply(this.#store, arguments)
  }

  update() {
    return this.#store.update.apply(this.#store, arguments)
  }

  set() {
    return this.#store.set.apply(this.#store, arguments)
  }

  isEmpty() {
    return this.markers.length == 0
  }

  clear() {
    this.markers = initialMarkers
    this.emit('clear')
    this._updated()
  }

  all() {
    return this.markers || []
  }

  get(id) {
    return this.markers.find(m => m.id === id)
  }

  add(data) {
    let id = this._getNewId()
    let marker = { ...data, id: id }
    this.markers = [...this.markers, marker] 
    this.emit('add', marker)
    this._updated()
    return id
  }

  remove(id) {
    let marker = this.get(id)
    this.markers = this.markers.filter(m => m.id !== id)
    this.emit('remove', marker)
    this._updated()
  }

  update(id, data) {
    const i = this.markers.findIndex(m => m.id === id)
    this.markers[i] = { ...this.markers[i], ...data }
    this.emit('update', this.markers[i])
    this._updated()
  }

  replace(id, data) {
    const i = this.markers.findIndex(m => m.id === id)
    this.markers[i] = { id: id, ...data }
    this.emit('update', this.markers[i])
    this._updated()
  }

  export() {
    return JSON.stringify(this.markers)
  }

  import(markers) {
    this.markers = typeof(markers) == 'string' ? JSON.parse(markers) : markers
    this.emit('clear')
    this._updated()
  }

  _getNewId() {
    return this.markers.length > 0 ? Math.max(...this.markers.map(m => m.id)) + 1 : 1
  }

  _updated() {
    this.#store.set(this.markers)
    this.emit('change')
  }
}

const markerStore = new MarkerStore()

export default markerStore