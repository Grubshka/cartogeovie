import { writable } from 'svelte/store'
import { localStore } from './localStore'

const print = writable()
const addMode = writable()
const showPopups = writable()
const markerList = writable()

const editingMarker = writable()
const selectedMarker = writable()
const editingMarkerSet = localStore('ladapt-current-markerset', '')

const infoModalOpened = writable()

export { print, addMode, showPopups, markerList, editingMarker, selectedMarker, editingMarkerSet, infoModalOpened }