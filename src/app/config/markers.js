const config = {
  markers: {

    colors: {
      default: 'blue',
      black: 'black',
      red: 'red',
      green: 'lime'
    },

    icons: {
      default: 'fas fa-circle',
      home: 'fas fa-home',
      work: 'fas fa-briefcase',
      hobbies: 'fas fa-futbol',
      groceries: 'fas fa-shopping-cart',
      shopping: 'fas fa-shopping-bag',
      children: 'fas fa-baby',
      family: 'fas fa-house-user',
      friends: 'fas fa-user-friends',
      health: 'fas fa-medkit',
      administrative: 'fas fa-university',
      help: 'fas fa-hands-helping',
    }
  }
}

export default config