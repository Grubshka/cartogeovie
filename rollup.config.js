import svelte from 'rollup-plugin-svelte'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import livereload from 'rollup-plugin-livereload'
import browsersync from 'rollup-plugin-browsersync'
import { terser } from 'rollup-plugin-terser'
import sveltePreprocess from 'svelte-preprocess'
import css from 'rollup-plugin-css-only'
import copy from 'rollup-plugin-copy'
import babel from '@rollup/plugin-babel'
import postcss from 'rollup-plugin-postcss'
import json from "@rollup/plugin-json"

const production = !process.env.ROLLUP_WATCH;

function serve() {
	let server;
	
	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;
			server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
				stdio: ['ignore', 'inherit', 'inherit'],
				shell: true
			});

			process.on('SIGTERM', toExit);
			process.on('exit', toExit);
		}
	};
}

export default {
	input: 'src/app/main.js',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: 'public/bundle.js'
	},
	plugins: [
    json(),
    copy({
      targets: [
        { src: 'src/index.html', dest: 'public' },
        { src: 'src/assets/images/**/*', dest: 'public/images' },
        // { src: 'node_modules/leaflet/dist/images/*', dest: 'public/images' },
        { src: 'src/assets/fonts/*', dest: 'public/fonts' },
        { src: 'src/assets/images/favicon.png', dest: 'public' },
      ]
    }),
    css({ output: 'public/bundle.css' }),
		svelte({
			// enable run-time checks when not in production
			dev: !production,
			// we'll extract any component CSS out into
			// a separate file - better for performance
      css: css => {
       	css.write('bundle.css');
			},
      preprocess: sveltePreprocess({
        scss: {
          includePaths: ['src'],
        },
        postcss: {
          plugins: [
            require('postcss-import')(),
            require('autoprefixer')(),
          ]
        },
      }),
    }),
    postcss({
      plugins: [
        require('postcss-import')(),
        require('autoprefixer')(),
      ]
    }),
    babel({
      extensions: [ ".js", ".mjs", ".html", ".svelte" ],
      // presets: [["@babel/env", { "targets": "ie 9" }]],
      plugins: ["@babel/plugin-proposal-class-properties", "@babel/plugin-proposal-private-methods"]
    }),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),

		// In dev mode, call `npm run start` once
		// the bundle has been generated
		!production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		// !production && livereload('public'),
		!production && browsersync({ server: 'public'}),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser()
	],
	watch: {
    clearScreen: false,
    chokidar: false
	}
};
