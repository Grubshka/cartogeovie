# Cartogéovie

Projet développé avec [Svelte](https://svelte.dev).

## Développement

Nécessite Node.js.

Installer les dépendances...

```bash
npm install
```

...et c'est parti

```bash
npm run dev
```

Ouvrir [localhost:5000](http://localhost:5000) dans votre navigateur.

If you're using [Visual Studio Code](https://code.visualstudio.com/) we recommend installing the official extension [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode). If you are using other editors you may need to install a plugin in order to get syntax highlighting and intellisense.

## Compilation pour le déploiement en production

```bash
npm run build
```

## Déploiement

Copier le contenu du répertoire `public` sur votre hébergement.
